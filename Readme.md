# DBIC-UPS
** Uninterruptible Power Supply Module
-----------------------------------
### The software part for "Uninterruptible Power Supply Module", the opportunities of:
* 5 PoE 24V for video cameras to 48V PoE
* 3 ch 48V for network switches to 24V
* 2 ch 12V for AVER
* monitoring the current battery charge 24V
* voltage monitoring for video cameras to 48V PoE
* voltage monitoring for network switches to 24V
* monitoring of the above stresses and timely notice of the loss of the corresponding denomination on RS232 and on the LCD
* record logging in EEPROM
* check the temperature of the battery and timely notice of increase
-----------------------------------
# About
### Hardware
    ATmega 128
    DS18B20
    Solid State Relays
-----------------------------------

# Configuration port, timers and more...in file initialization/general.h
### Port initialization
	/********************
		PORT INIT 
	********************/
	void port_init(void) {
		PORTF = 0x00;
		DDRF  = 0x00;
		PORTB = 0x00;
		DDRB  = 0xff;
	}
### File: initialization/general.h Twi initialization
	/********************
		TWI initialize
		bit rate:18 
		(freq: 100Khz) 
	********************/
	void twi_init(void) {
		TWCR= 0X00;								// disable twi
		TWBR= 0x12;								// set bit rate
		TWSR= 0x00;								// set prescale
		TWCR= 0x44;								// enable twi
	}
-----------------------------------
# Configuration pin in file structures/general_structures.h
	#define RELAY_PARALLEL_ON		PORTB |= (1<<5)
	#define RELAY_PARALLEL_OFF		PORTB &= ~(1<<5)
	#define RELAY_POWER_ON			PORTB |= (1<<6)
	#define RELAY_POWER_OFF			PORTB &= ~(1<<6)
	#define ADC_VREF_TYPE 0x00
-----------------------------------
# Configuration message locale in file locale/en.h
	// LCD MESSAGE
	#define LCDWelcome				"WELCOME TO DBIC-UPS"
	#define LCDVersion				"Version: 1.0.1"
	#define LCDNameBoard			"DBIC-UPS v.1.0.1"
	#define LCDAboutBoard			"By KB PROMSVYAZ"
	#define LCDAddressBoard			"st. Chaplin 71"
	#define LCDwwwBoard				"www.dbic.pro"
	#define PV48					"Power V48<48V"
	#define PV24					"Power V24<24V"
	#define PV5						"Power V5<5V"
	#define PBAT					"Battery < 24V"
	#define PACC                    "Charging...  "
	#define EMPTY                   "             "

	// UART MESSAGE
	#define UARTWelcome				">-------- WELCOME TO DBIC-UPS"
	#define UARTVersion				">-------- Version: 1.0.1"
	#define UARTAboutBoard			">-------- By \"KB PROMSVYAZ\""
	#define UARTToContinued			"> Please press \"Enter\" to continue"
	#define UARThr					">---------------------------------------"
	// UART MESSAGE MENU
	#define UARTMenu0               "> 0 : Display RTC Date\0"
	#define UARTMenu1               "> 1 : Display RTC Time\0"
	#define UARTMenu2               "> 2 : Update RTC Date\0"
	#define UARTMenu3               "> 3 : Update RTC Time\0"
	#define UARTMenu4               "> 4 : EEPROM LOG"
	#define UARTMenu9               "> 9 : Test work"
	#define UARTMenu10              "> Select Option (0-9): "

	#define UARTInvOptions          " Invalid option!"
	#define UARTEepromFailed        " EEPROM operation failed.."
	#define UARTContinued           " Finished.. Please press \"Enter\" to continue"
-----------------------------------
### More functions:
	checkAccumulate ();	// Check the battery status and the presence of the electric power
    RTC_lcdDate ();   	// Form the date after which to give standarntny conclusion
    RTC_lcdTime ();		// Form the time after which to give standarntny conclusion
    LCDupdate ();		// Form the message and give to the standard output and display:
-----------------------------------