/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: RTC_EEPROM_main.c
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

//#ifdefined GENERAL_LIBRARIES
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
//#endif
#include "locale/en.h"		// Default language to message/errors/warnings
#include "structures/general_sctrunctures.h"	// All define/include/struct
#include "functions/UART_routines.h"	// Library for serial port
#include "drivers/i2c_routines.h"	// Library for i2c

#include "functions/RTC_routines.h"	// Libraries real-time clock
#include "functions/EEPROM_routines.h"	// Libraries and event recording to internal memory
#include "functions/ADC_routines.h"	// Libraries for processing data from the ADC
#include "lcd/lcd_lib.h"	// Libraries for use with an external display
#include "sensors/DS18B20_routines.h"	// Library to work with temperature sensors
#include "initialization/general.h"	// Library with a standard description of all functions
#include "interrupt/rs232.h"	// Library for serial port interrupts
#include "eeprom/eeprom_address.h"	// Library addresses the internal memory
#include "functions/checkAccumulate.h"	// Library check the battery power
#include "functions/message.h"	// The message of the library error/warning
#include "functions/eemem_log.h"	// Library for logging data in the internal memory

/*ISR(TIMER1_COMPA_vect)
{
	tot_overflow++;												// keep a track of number of overflows
    
    cli();														// Disallow all interrupts
    if (tot_overflow == 7200) {									// NOTE: '>=' used instead of '=='
        //tempTemperature = temp_18b20();						  // Check temperature ds18b20
        
		RELAY_POWER_OFF;										// Off rellay power battery
        if(adc_result3>=12) {
                RELAY_PARALLEL_OFF;								// Off parallel relay
        }
        tot_overflow=0;											// Reset timer count
	}
	sei();														// Allow all interrupts
}*/

/********************
	MAIN FUNCTION 
********************/
int
main (void)
{
  int i;
  init_devices ();		// Initialization all settings
  LCDclr ();			// Clear LCD
  LCDGotoXY (0, 0);
  LCDstring (LCDWelcome, strlen (LCDWelcome));	// Print message: Welcome
  LCDGotoXY (0, 2);
  LCDstring (LCDVersion, strlen (LCDVersion));	// Print message: Version
  for (i = 0; i <= 19; i++)
    {
      LCDGotoXY (i, 3);
      LCDstring ("*", 1);
      _delay_ms (200);		// Print message: Progrssbar
    }
  _delay_ms (1000);

  LCDclr ();			// Clear LCD
  LCDGotoXY (0, 0);
  LCDstring (LCDNameBoard, strlen (LCDNameBoard));      	// Print message: Name board
  LCDGotoXY (0, 1);
  LCDstring (LCDAboutBoard, strlen (LCDAboutBoard));	        // Print message: About info
  LCDGotoXY (0, 2);
  LCDstring (LCDAddressBoard, strlen (LCDAddressBoard));	// Print message: Address board
  LCDGotoXY (0, 3);
  LCDstring (LCDwwwBoard, strlen (LCDwwwBoard));        	// Print message: WWW address board
  _delay_ms (2000);
  LCDclr ();
  _delay_ms (500);	                	// Delay & LCD Clear & delay

  TX_NEWLINE;
  TX_NEWLINE;		                	// Print UART message: \n
  TX_NEWLINE;
  transmitString_F (PSTR (UARThr));     	// Print UART message: hr
  TX_NEWLINE;
  transmitString_F (PSTR (UARTWelcome));	// Print UART message: Welcome
  TX_NEWLINE;
  transmitString_F (PSTR (UARTVersion));	// Print UART message: Version
  TX_NEWLINE;
  transmitString_F (PSTR (UARTAboutBoard));	// Print UART message: About info
  TX_NEWLINE;
  transmitString_F (PSTR (UARThr));	        // Print UART message:
  TX_NEWLINE;
  transmitString_F (PSTR (UARTToContinued));	// Print UART message: Continued ...

   /************************
    TITLE MESSAGE TO UART 
	------------------------
	END SEND MESSAGE
	***********************/
  while (1)
    {
      checkAccumulate ();	// Check the battery status and the presence of the electric power
      RTC_lcdDate ();   	// Form the date after which to give standarntny conclusion
      RTC_lcdTime ();		// Form the time after which to give standarntny conclusion
      LCDupdate ();		// Form the message and give to the standard output and display:
    }
  return 0;
}

/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: RTC_EEPROM_main.c
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
