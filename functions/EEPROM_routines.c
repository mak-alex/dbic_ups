/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: EEPROM_routines.c
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

#include <avr/io.h>
#include <avr/pgmspace.h>
#include "functions/EEPROM_routines.h"
#include "functions/UART_routines.h"
#include "drivers/i2c_routines.h"


//******************************************************************
//Function to read given number of bytes from EEPROM 
//******************************************************************
unsigned char EEPROM_read(unsigned char highAddress, unsigned char lowAddress, unsigned char totalChar)
{
   unsigned char errorStatus, i, data;
   
   errorStatus = i2c_start();
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("i2c start failed.."));
   	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendAddress(EEPROM_W);
   
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM sendAddress1 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendData(highAddress);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM write-1 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
 
   errorStatus = i2c_sendData(lowAddress);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM write-2 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_repeatStart();
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("i2c repeat-start failed.."));
	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendAddress(EEPROM_R);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM sendAddress2 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
   for(i=0;i<totalChar;i++)
   {
      if(i == (totalChar-1))  	 //no Acknowledge after receiving the last byte
	   	  data = i2c_receiveData_NACK();
	  else
	  	  data = i2c_receiveData_ACK();
		  
   	  if(data == ERROR_CODE)
   	  {
       		transmitString_F(PSTR("EEPROM receive failed.."));
			i2c_stop();
	   		return(1);
   	  }
	   
	   if((i%16) == 0) 
	     TX_NEWLINE;
	   transmitByte(data);
	   
   }
   
   TX_NEWLINE;
   i2c_stop();
   
   return(0);
}

//******************************************************************
//Function to a character string to EEPROM 
//******************************************************************
unsigned char EEPROM_write(unsigned char highAddress, unsigned char lowAddress)
{
   unsigned char errorStatus, data;
   
   errorStatus = i2c_start();
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("i2c start failed.."));
   	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendAddress(EEPROM_W);
   
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM sendAddress1 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendData(highAddress);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM write-1 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
 
   errorStatus = i2c_sendData(lowAddress);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM write-2 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
    while((data = receiveByte()) != 0x0d)
   {
      transmitByte(data);
	  if(data == 0x0d)
 	  transmitByte(0x0a);
	  errorStatus = i2c_sendData(data);
   	  if(errorStatus == 1)
   	  {
       		transmitString_F(PSTR("EEPROM write data failed.."));
			i2c_stop();
	   		return(1);
   	  }
   }
   
   i2c_stop();
   
   return(0);
}

//******************************************************************
//Function to read one page of EEPROM 
//******************************************************************
unsigned char EEPROM_readPage( unsigned int pageNumber )
{
  unsigned char highAddress, lowAddress, errorStatus;
  unsigned int pageAddress;
  
  pageAddress = pageNumber * 64;
  
  highAddress = (unsigned char)((pageAddress >> 8) & 0x00ff);
  lowAddress  = (unsigned char)( pageAddress & 0x00ff);
  
  errorStatus = EEPROM_read(highAddress, lowAddress, 64);
  
  return(errorStatus);
 } 
  
//******************************************************************
//Function to write one page of EEPROM 
//******************************************************************
unsigned char EEPROM_writePage( unsigned int pageNumber )
{
  unsigned char highAddress, lowAddress, errorStatus;
  unsigned int pageAddress;
  
  pageAddress = pageNumber * 64;
  
  highAddress = (unsigned char)((pageAddress >> 8) & 0x00ff);
  lowAddress  = (unsigned char)( pageAddress & 0x00ff);
  
  errorStatus = EEPROM_write(highAddress, lowAddress);
  
  return(errorStatus);  
} 
 
//******************************************************************
//Erasing EEPROM (filling it with databyte 0xff)
//******************************************************************
unsigned char EEPROM_erase(void)
{
  
  unsigned char errorStatus;
  unsigned int i;
  
  TX_NEWLINE;
  TX_NEWLINE;
  transmitString_F(PSTR("Wait..."));
  TX_NEWLINE;
   
   errorStatus = i2c_start();
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("i2c start failed.."));
   	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendAddress(EEPROM_W);
   
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM sendAddress1 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
   errorStatus = i2c_sendData(0x00);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM write-1 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
 
   errorStatus = i2c_sendData(0x00);
   if(errorStatus == 1)
   {
     transmitString_F(PSTR("EEPROM write-2 failed.."));
	 i2c_stop();
	 return(1);
   } 
   
    for(i=0;i<0x8000;i++)
   {
	  errorStatus = i2c_sendData(0xff);
   	  if(errorStatus == 1)
   	  {
       		transmitString_F(PSTR("EEPROM write data failed.."));
			i2c_stop();
	   		return(1);
   	  }
   }
   
   i2c_stop();
   
   return(0);

}   	  

/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: EEPROM_routines.c
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
