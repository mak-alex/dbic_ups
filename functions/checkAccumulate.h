/*****************************************************************************
 *
 *                        AVR UPS
 *  FILENAME: checkAccumulate.h
 *  This file is part of the AVR UPS project. 
 *  The project is distributed at:
 *  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
 *
 *  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
 *
 *  This software is licensed under the following license (Modified BSD
 *  License):
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. The name of the author may not be used to endorse or promote
 *      products derived from this software without specific prior written
 *      permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

/********************
 *	CHECK POWER
 *	-----------
 *	+BATTERY
 *	+BOARD
 *	+CAMMERA 
 ********************/
extern char time[10], date[12];	// Extern buffer to RTC
void
checkAccumulate (void)
{
  int i = 0;
  adc_result0 = (adc_read (0) / 205) * 2.5;	// Calculation of 5V
  adc_result1 = adc_read (3) / 40;	// Calculation of 24V
  adc_result2 = adc_read (1) / 15.8;	// Calculation of 48V
  //adc_result3 = adc_read(2)/40;                   // Calculation of 24V

  /*********************
   * -CHECK STATE BATTERY
   * -SWITCH RELAY TO PARALLEL
   * -SWITCH RELAY TO POWER BAT
   ********************/
  if (temp_adc_result3 == 0)
    {
      adc_result3 = adc_read (2) / 40;	// New value voltage
      temp_adc_result3 = adc_result3;	// Old value voltage
    }
  else
    {
      temp_adc_result3 = adc_result3;	// Old value voltage
      adc_result3 = adc_read (2) / 40;	// New value voltage
    }
  if (((adc_result3 - temp_adc_result3) <= 5) & (adc_result1 >= 24))
    {
      LCDGotoXY (0, 1);
      LCDstring (PACC, strlen (PACC));
      RELAY_PARALLEL_ON;	// On rellay parallel
      RELAY_POWER_ON;		// On rellay power
    }
  else
    {
      RELAY_POWER_OFF;
      RELAY_PARALLEL_OFF;
    }

  //sprintf(log_rellay_on, "*::RELLAY::%s::%s::%s", time, date, ((adc_result3<20)&&(adc_result3>0)&&(adc_result1==24))?"ON":"OFF");
  //TX_NEWLINE; transmitString(log_rellay_on);
}

/*****************************************************************************
 *
 *                        AVR UPS
 *  FILENAME: checkAccumulate.h
 *  This file is part of the AVR UPS project. 
 *  The project is distributed at:
 *  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
 *
 *  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
 *
 *  This software is licensed under the following license (Modified BSD
 *  License):
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. The name of the author may not be used to endorse or promote
 *      products derived from this software without specific prior written
 *      permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/
