/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: message.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
void
LCDupdate (void)
{
  int i = 0;
  /*
   * The first line of the display
   */
  sprintf (v5, "%2u", adc_result0);	// Form result ADC
  LCDGotoXY (10, 3);
  LCDstring ("V5: ", 4);	// Print to LCD display
  LCDGotoXY (16, 3);
  LCDstring (v5, 2);		// Print result to LCD display
  /*
   * The second line of the display
   */
  sprintf (v24, "%2u", adc_result1);	// Form result ADC
  LCDGotoXY (0, 2);
  LCDstring ("V24: ", 5);	// Print to LCD display 
  LCDGotoXY (6, 2);
  LCDstring (v24, 2);		// Print result to LCD display
  /*
   * The third line of the display
   */
  sprintf (v48, "%2u", adc_result2);	// Form result ADC
  LCDGotoXY (10, 2);
  LCDstring ("V48: ", 5);	// Print to LCD display
  LCDGotoXY (16, 2);
  LCDstring (v48, 2);		// Print result to LCD display
  /*
   * The fourth line of the display
   */
  sprintf (bat, "%2u", adc_result3);	// Form result ADC
  LCDGotoXY (0, 3);
  LCDstring ("BAT: ", 5);	// Print to LCD display
  LCDGotoXY (6, 3);
  LCDstring (bat, 2);		// Print result to LCD display
	/*********************
	  -CHECK STATE POWER BOARD
	  -SEND ERR MESSAGE 
	 ********************/
  if (adc_result1 < 24)
    {
      sprintf (log_err1, "1::V24::%s::%s::%s", time, date, v24);	// Form a buffer for serial port
      TX_NEWLINE;
      transmitString (log_err1);	// Outputs the generated buffer to the serial port
      LCDGotoXY (0, 1);
      LCDstring (PV24, strlen (PV24));	// Outputs the generated display buffer
      _delay_ms (200);		// Delay 200 milliseconds
    }
  if (adc_result2 < 45)
    {
      sprintf (log_err2, "2::V48::%s::%s::%s", time, date, v48);	// Form a buffer for serial port
      TX_NEWLINE;
      transmitString (log_err2);	// Outputs the generated buffer to the serial port
      LCDGotoXY (0, 1);
      LCDstring (PV48, strlen (PV48));	// Outputs the generated display buffer
      _delay_ms (200);		// Delay 200 milliseconds
    }
  if (adc_result3 <= 20)
    {
      sprintf (log_err3, "0::BAT::%s::%s::%s", time, date, bat);	// Form a buffer for serial port
      TX_NEWLINE;
      transmitString (log_err3);	// Outputs the generated buffer to the serial port
      LCDGotoXY (0, 1);
      LCDstring (PBAT, strlen (PBAT));	// Outputs the generated display buffer
      _delay_ms (200);		// Delay 200 milliseconds
    }
  //sprintf(temperature, "%02iC", tempTemperature);                 // Form a buffer with a temperature
  //LCDGotoXY(17,0); LCDstring(temperature, 3);                     // Outputs the generated display buffer
}

/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: message.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
