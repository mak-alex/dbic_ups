var RTC__routines_8c =
[
    [ "RTC_displayDate", "d3/de8/RTC__routines_8c.html#af7aad9be2486c7d400f2cb819acfe8ec", null ],
    [ "RTC_displayDay", "d3/de8/RTC__routines_8c.html#a5ae5e3dd5f267bab31bba29d13495e4c", null ],
    [ "RTC_displayTime", "d3/de8/RTC__routines_8c.html#a25b79eb84c2ba8a981b791fbb73e85e7", null ],
    [ "RTC_getDate", "d3/de8/RTC__routines_8c.html#a7683e7bed46ff687d2bf9deea52222fd", null ],
    [ "RTC_getTime", "d3/de8/RTC__routines_8c.html#a360451ae891159c8c35f454baf921214", null ],
    [ "RTC_lcdDate", "d3/de8/RTC__routines_8c.html#ad5ef0a0878020b3c688c2e8ac4c53237", null ],
    [ "RTC_lcdTime", "d3/de8/RTC__routines_8c.html#a5d0136a963b23d30201759f988fe09a2", null ],
    [ "RTC_read", "d3/de8/RTC__routines_8c.html#a55d788e040f3b1612d852175541ec7c3", null ],
    [ "RTC_setStartAddress", "d3/de8/RTC__routines_8c.html#a44c02141a8a4c29d6dc3e30bf94650b3", null ],
    [ "RTC_updateDate", "d3/de8/RTC__routines_8c.html#ac7f7e6c87d816a02404c9219d435030b", null ],
    [ "RTC_updateRegisters", "d3/de8/RTC__routines_8c.html#a744006f211f8d54a931a49d64c9200da", null ],
    [ "RTC_updateTime", "d3/de8/RTC__routines_8c.html#af48e392540efde9b46a2d2de58a35de6", null ],
    [ "RTC_writeDate", "d3/de8/RTC__routines_8c.html#ac2abd7ab651570b394e59c8d52d74c88", null ],
    [ "RTC_writeTime", "d3/de8/RTC__routines_8c.html#a387bf35dae35bd2fa110205411ec118b", null ],
    [ "date", "d3/de8/RTC__routines_8c.html#a6884eac501df3f0aa53974eb39f1c71a", null ],
    [ "day", "d3/de8/RTC__routines_8c.html#adda1600ac8d0e82020ae534616236f43", null ],
    [ "rtc_register", "d3/de8/RTC__routines_8c.html#a3d0e688063698375ee0ce9d0414f1533", null ],
    [ "time", "d3/de8/RTC__routines_8c.html#a695945b42d6900a677194a1d3109563b", null ]
];