var i2c__routines_8c =
[
    [ "i2c_receiveData_ACK", "de/d4d/i2c__routines_8c.html#abaf61bedd7d52c0b98498245e761c888", null ],
    [ "i2c_receiveData_NACK", "de/d4d/i2c__routines_8c.html#a1868782261614a66d6e9a3d098fa9a15", null ],
    [ "i2c_repeatStart", "de/d4d/i2c__routines_8c.html#ad8a19f1dc7c4cd11b15a47b32a59ffa6", null ],
    [ "i2c_sendAddress", "de/d4d/i2c__routines_8c.html#ab160063b10c389a3a10559f2c2b4c1e9", null ],
    [ "i2c_sendData", "de/d4d/i2c__routines_8c.html#aed93db58542cbc20fb77b6a6b97f2b1a", null ],
    [ "i2c_start", "de/d4d/i2c__routines_8c.html#a03940a22b033dd8f1468ad7cb1957118", null ],
    [ "i2c_stop", "de/d4d/i2c__routines_8c.html#ad35d4e4f52ca74b503d5e5e1e0a3f5f3", null ]
];