var RTC__routines_8h =
[
    [ "DATE", "d4/dcb/RTC__routines_8h.html#a30b328ca499f27b3d0f8111b495834ca", null ],
    [ "DAY", "d4/dcb/RTC__routines_8h.html#a509a01c55cbe47386fe24602b7c7fda1", null ],
    [ "HOURS", "d4/dcb/RTC__routines_8h.html#a212d0f839f6f7ca43ccde311f93d5892", null ],
    [ "MINUTES", "d4/dcb/RTC__routines_8h.html#a84be9dcfa5a172ee83121620d15c8e29", null ],
    [ "MONTH", "d4/dcb/RTC__routines_8h.html#a3729d06495d9713592f79f3122c9e677", null ],
    [ "SECONDS", "d4/dcb/RTC__routines_8h.html#a48fcf4f2eeef6769d588168d4ac2ab0e", null ],
    [ "YEAR", "d4/dcb/RTC__routines_8h.html#a5871356500f559add06ea81d60331b1b", null ],
    [ "RTC_displayDate", "d4/dcb/RTC__routines_8h.html#af7aad9be2486c7d400f2cb819acfe8ec", null ],
    [ "RTC_displayDay", "d4/dcb/RTC__routines_8h.html#a5ae5e3dd5f267bab31bba29d13495e4c", null ],
    [ "RTC_displayTime", "d4/dcb/RTC__routines_8h.html#a25b79eb84c2ba8a981b791fbb73e85e7", null ],
    [ "RTC_getDate", "d4/dcb/RTC__routines_8h.html#a7683e7bed46ff687d2bf9deea52222fd", null ],
    [ "RTC_getTime", "d4/dcb/RTC__routines_8h.html#a360451ae891159c8c35f454baf921214", null ],
    [ "RTC_read", "d4/dcb/RTC__routines_8h.html#a55d788e040f3b1612d852175541ec7c3", null ],
    [ "RTC_setStartAddress", "d4/dcb/RTC__routines_8h.html#a44c02141a8a4c29d6dc3e30bf94650b3", null ],
    [ "RTC_updateDate", "d4/dcb/RTC__routines_8h.html#ac7f7e6c87d816a02404c9219d435030b", null ],
    [ "RTC_updateRegisters", "d4/dcb/RTC__routines_8h.html#a744006f211f8d54a931a49d64c9200da", null ],
    [ "RTC_updateTime", "d4/dcb/RTC__routines_8h.html#af48e392540efde9b46a2d2de58a35de6", null ],
    [ "RTC_write", "d4/dcb/RTC__routines_8h.html#a6dea28aef3ddbdc55ec0348aaa94b8fe", null ],
    [ "RTC_writeDate", "d4/dcb/RTC__routines_8h.html#ac2abd7ab651570b394e59c8d52d74c88", null ],
    [ "RTC_writeTime", "d4/dcb/RTC__routines_8h.html#a387bf35dae35bd2fa110205411ec118b", null ]
];