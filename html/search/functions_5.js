var searchData=
[
  ['i2c_5freceivedata_5fack',['i2c_receiveData_ACK',['../de/d4d/i2c__routines_8c.html#abaf61bedd7d52c0b98498245e761c888',1,'i2c_receiveData_ACK(void):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#abaf61bedd7d52c0b98498245e761c888',1,'i2c_receiveData_ACK(void):&#160;i2c_routines.c']]],
  ['i2c_5freceivedata_5fnack',['i2c_receiveData_NACK',['../de/d4d/i2c__routines_8c.html#a1868782261614a66d6e9a3d098fa9a15',1,'i2c_receiveData_NACK(void):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#a1868782261614a66d6e9a3d098fa9a15',1,'i2c_receiveData_NACK(void):&#160;i2c_routines.c']]],
  ['i2c_5frepeatstart',['i2c_repeatStart',['../de/d4d/i2c__routines_8c.html#ad8a19f1dc7c4cd11b15a47b32a59ffa6',1,'i2c_repeatStart(void):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#ad8a19f1dc7c4cd11b15a47b32a59ffa6',1,'i2c_repeatStart(void):&#160;i2c_routines.c']]],
  ['i2c_5fsendaddress',['i2c_sendAddress',['../de/d4d/i2c__routines_8c.html#ab160063b10c389a3a10559f2c2b4c1e9',1,'i2c_sendAddress(unsigned char address):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#a50ec51353201fdd458565aaa1dd498e7',1,'i2c_sendAddress(unsigned char):&#160;i2c_routines.c']]],
  ['i2c_5fsenddata',['i2c_sendData',['../de/d4d/i2c__routines_8c.html#aed93db58542cbc20fb77b6a6b97f2b1a',1,'i2c_sendData(unsigned char data):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#ace6cc3ae36dc22ea157c6440650ed572',1,'i2c_sendData(unsigned char):&#160;i2c_routines.c']]],
  ['i2c_5fstart',['i2c_start',['../de/d4d/i2c__routines_8c.html#a03940a22b033dd8f1468ad7cb1957118',1,'i2c_start(void):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#a03940a22b033dd8f1468ad7cb1957118',1,'i2c_start(void):&#160;i2c_routines.c']]],
  ['i2c_5fstop',['i2c_stop',['../de/d4d/i2c__routines_8c.html#ad35d4e4f52ca74b503d5e5e1e0a3f5f3',1,'i2c_stop(void):&#160;i2c_routines.c'],['../d8/d49/i2c__routines_8h.html#ad35d4e4f52ca74b503d5e5e1e0a3f5f3',1,'i2c_stop(void):&#160;i2c_routines.c']]],
  ['init_5fdevices',['init_devices',['../d9/d14/general_8h.html#a2e22d5e270a39ffcf36b38e02cc7b033',1,'general.h']]],
  ['inittimers',['initTimers',['../d9/d14/general_8h.html#a87611108f72b2495e59f965bd53ac7cf',1,'general.h']]],
  ['isr',['ISR',['../dd/d8b/rs232_8h.html#ae6e8a8009a9ae0c59f25a496d1cf5a84',1,'rs232.h']]]
];
