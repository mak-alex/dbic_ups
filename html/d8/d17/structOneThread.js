var structOneThread =
[
    [ "blocked", "d8/d17/structOneThread.html#af9b64f65f187bcee12d118e15ee30cbb", null ],
    [ "delay", "d8/d17/structOneThread.html#a72af283d06cb2010cd291d049eb7c896", null ],
    [ "parameter", "d8/d17/structOneThread.html#a2b92a2c4afc6a3cbd97b585866f0887d", null ],
    [ "ready", "d8/d17/structOneThread.html#a2554f619b27ef0eabec56ec0f5c17158", null ],
    [ "reserved_1", "d8/d17/structOneThread.html#aed9514b6f698ccf6462bd8b35a6adbad", null ],
    [ "reserved_2", "d8/d17/structOneThread.html#ab6131d34a966e2f5277d9c9e09699126", null ],
    [ "reserved_3", "d8/d17/structOneThread.html#a4a7a98f8113a39d5c8955e1944771332", null ],
    [ "sectionId", "d8/d17/structOneThread.html#a102b5a46c18ff22a0993d38a1d2b2e5c", null ],
    [ "sleeped", "d8/d17/structOneThread.html#a51e331b4f838e1bedd8ea040695b3469", null ],
    [ "stackBottom", "d8/d17/structOneThread.html#ab64d715473047dc62bbe81f48f14d101", null ],
    [ "stackPointer", "d8/d17/structOneThread.html#a99bf8d869d7f127e0f1aacb17220a0ce", null ],
    [ "suspended", "d8/d17/structOneThread.html#af1ac5d67a7bacd1b4a4346143cc8fb11", null ],
    [ "waited", "d8/d17/structOneThread.html#a205b6d33d1a0a05f5ecb38b85655417d", null ],
    [ "waitMask", "d8/d17/structOneThread.html#a3d6b4fc5cf2301aedac46d26a0d3d147", null ]
];