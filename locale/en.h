/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: checkAccumulate.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

// LCD MESSAGE
#define LCDWelcome				"WELCOME TO DBIC-UPS"
#define LCDVersion				"Version: 1.0.1"
#define LCDNameBoard			"DBIC-UPS v.1.0.1"
#define LCDAboutBoard			"By KB PROMSVYAZ"
#define LCDAddressBoard			"st. Chaplin 71"
#define LCDwwwBoard				"www.dbic.pro"
#define PV48					"Power V48<48V"
#define PV24					"Power V24<24V"
#define PV5						"Power V5<5V"
#define PBAT					"Battery < 24V"
#define PACC                    "Charging...  "
#define EMPTY                   "             "

// UART MESSAGE
#define UARTWelcome				">-------- WELCOME TO DBIC-UPS"
#define UARTVersion				">-------- Version: 1.0.1"
#define UARTAboutBoard			">-------- By \"KB PROMSVYAZ\""
#define UARTToContinued			"> Please press \"Enter\" to continue"
#define UARThr					">---------------------------------------"
// UART MESSAGE MENU
#define UARTMenu0               "> 0 : Display RTC Date\0"
#define UARTMenu1               "> 1 : Display RTC Time\0"
#define UARTMenu2               "> 2 : Update RTC Date\0"
#define UARTMenu3               "> 3 : Update RTC Time\0"
#define UARTMenu4               "> 4 : EEPROM LOG"
#define UARTMenu9               "> 9 : Test work"
#define UARTMenu10              "> Select Option (0-9): "

#define UARTInvOptions          " Invalid option!"
#define UARTEepromFailed        " EEPROM operation failed.."
#define UARTContinued           " Finished.. Please press \"Enter\" to continue"

/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: checkAccumulate.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
