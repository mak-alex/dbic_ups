/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: DS18B20_routines.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

#define W1_PORT PORTD
#define W1_DDR DDRD
#define W1_PIN PIND
#define W1_BIT 4
/********************
	FIND ONEWIRE 
********************/
unsigned char w1_find()
{
	unsigned char device;
	//PORTC = 0xFF;
	W1_DDR |= 1<<W1_BIT;
	_delay_us(485);
	//PORTC = 0x00;
	W1_DDR &= ~(1<<W1_BIT);
	_delay_us(65);
	
	if((W1_PIN & (1<<W1_BIT)) ==0x00)
		device = 1;
	else
		device = 0;
	_delay_us(420);
	return device;
}
/********************
	SEND CMD ONEWIRE 
********************/
void w1_sendcmd(unsigned char cmd)
{
	unsigned char i;
	for(i = 0; i < 8; i++)
	{
		if((cmd & (1<<i)) == 1<<i)
		{
			W1_DDR |= 1<<W1_BIT;
			_delay_us(2);
			W1_DDR &= ~(1<<W1_BIT);			
			_delay_us(65);
		} else {
			
			W1_DDR |= 1<<W1_BIT;
			_delay_us(65);
			W1_DDR &= ~(1<<W1_BIT);
			_delay_us(5);			
		}
	}
}
/********************
	RECEIVE BYTE 
	TO ONEWIRE 
********************/
unsigned char w1_receive_byte()
{
	unsigned char i;
	unsigned char data=0;
	for(i = 0; i < 8; i++)
	{	
		W1_DDR |= 1<<W1_BIT;
		_delay_us(2);
		W1_DDR &= ~(1<<W1_BIT) ;
		_delay_us(7);
		if((W1_PIN & (1<<W1_BIT)) == 0x00)
			data &= ~(1<<i);
		else
			data |= 1<<i;
		_delay_us(50);
	}
	return data;
}
/********************
	CHECK DS18B20 
********************/
int temp_18b20()
{
	unsigned char data[2];
	unsigned int temp = 0;
	
	if(w1_find()==1)
	{
		w1_sendcmd(0xcc);
		w1_sendcmd(0x44);
		_delay_ms(750);
		w1_find();
		w1_sendcmd(0xcc);
		w1_sendcmd(0xbe);
		data[0] = w1_receive_byte();
		data[1] = w1_receive_byte();

		temp = data[1];
		temp = temp<<8;
		temp |= data[0];

		temp *= 0.0625;
	}

	return temp;
}

/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: DS18B20_routines.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
