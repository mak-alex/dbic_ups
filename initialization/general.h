/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: initialization/general.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

/********************
	PORT INIT 
********************/
void port_init(void) {
	PORTF = 0x00;
	DDRF  = 0x00;
	PORTB = 0x00;
	DDRB  = 0xff;
}
/********************
	TWI initialize
	bit rate:18 
	(freq: 100Khz) 
********************/
void twi_init(void) {
	TWCR= 0X00;								// disable twi
	TWBR= 0x12;								// set bit rate
	TWSR= 0x00;								// set prescale
	TWCR= 0x44;								// enable twi
}

void initTimers() {
	/********************
	 	 INIT TIMER0
	 *******************/
	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 11059,200 kHz
	// Mode: Normal top=0xFFFF
	// OC1A output: Discon.
	// OC1B output: Discon.
	// OC1C output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer1 Overflow Interrupt: On
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: On
	// Compare B Match Interrupt: Off
	// Compare C Match Interrupt: Off
	TCCR1A=0x00;
	TCCR1B=0x01;
	TCNT1H=0x00;
	TCNT1L=0x00;
	ICR1H=0x00;
	ICR1L=0x00;
	OCR1AH=0x00;
	OCR1AL=0x00;
	OCR1BH=0x00;
	OCR1BL=0x00;
	OCR1CH=0x00;
	OCR1CL=0x00;

	TIMSK = 0x10;							//timer interrupt sources
}
//call this routine to initialize all peripherals
/********************
	INIT DEVICES 
********************/
void init_devices(void) {
	//stop errant interrupts until set up
	cli();									// Disallow all interrupts
	//XDIV  = 0x00;							  //xtal divider
	//XMCRA = 0x00;							  //external memory
	port_init();							// Init port state
	uart1_init();							// Init uart 1
	adc_init();								// Init ADC
	twi_init();								// Init TWI
	LCDinit();								// Init LCD display
	LCDclr();								// Clear LCD display
	LCDcursorOFF();							// Set off cursor to LCD display
	/*MCUCR = 0x00;
	EICRA = 0x00;							//extended ext ints
	EICRB = 0x00;							//extended ext ints
	EIMSK = 0x00;

	ETIMSK = 0x00;							//extended timer interrupt sources*/
	// tot_overflow = 1;

	// initTimers();
 
	// sei();								  // Allow all interrupts
} 

/*****************************************************************************
*
*                        AVR UPS
*  FILENAME: initialization/general.h
*  This file is part of the AVR UPS project. 
*  The project is distributed at:
*  http://enlab.su/stat/ups_s_distancionnym_upravleniem.htm
*
*  Copyright (c) 2014, Alex M.A.K. aka FlashHacker All rights reserved.
*
*  This software is licensed under the following license (Modified BSD
*  License):
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the author may not be used to endorse or promote
*      products derived from this software without specific prior written
*      permission.
*
*  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
*  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
*  NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
*  NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/
