//
//		(�) 2006, �.�.�����������. ��������� ���������� ����������� ������������ ���������� ����� ����������.
//
//		������ ����������� ����������������.
//
//		������ ������������ �� 8 ����������� ���������� ������� (�� ��� ���� "��������������" �����, ����� ��������� ����� ������).
// ������������� ��������������� ����� ��������� ����������, ����������� ������, �������� ������� ��� �������������� �� ��������
// ������������, � �� ������ ������������ ������� �� ������ ����� (�������� Sleep).
//
//		������: 1.00 �� 21.05.2006�
//
//		E-Mail: chav1961@mail.ru, chav1961@radioliga.com
//		����  : http://chav1961.narod.ru
//

typedef void ThreadProc(void *parameter);		// ��������� ������ ������.

extern void mt_firstThread(unsigned short stackSize,ThreadProc *proc,void *parameter);	// ������� �������������� � ������ �������������� ������.
extern unsigned char mt_newThread(ThreadProc *proc,void *parameter);	// ������� �������� ������ ������.
extern void mt_deleteThread(unsigned char threadId);			// ������� ���������� ���������� ������.
extern unsigned char mt_currentThread(void);					// ������� ������ �������������� �������� ������.
extern void mt_suspendThread(unsigned char threadId);		// ������� ��������� ��������� ���������� ������.
extern void mt_resumeThread(unsigned char threadId);			// ������� ������������� ����� �������������� ������.
extern void mt_prepareSection(unsigned char *sectionAddr);	// ������� ���������� � ������ ����� ����������� ������
extern void mt_lock(unsigned char *sectionAddr);				// ���������� ����������� ������.
extern void mt_unlock(unsigned char *sectionAddr);			// ����� �� ����������� ������.
extern void mt_prepareEvent(unsigned short wait);			// ���������� ���������� ������� � ��������� �������� �������������.
extern unsigned short mt_wait(unsigned short wait);			// �������� ���������� �������� �����/������ (�������).
extern void mt_notify(unsigned short wait);					// ������� ���������� �������.
extern void mt_sleep(unsigned short second_cent);			// �������� ��������� ���������� ���������.
extern void mt_yield(void);									// ������������ ���������� �� ������ �����.
extern void __mt_yield(void);									// ���������� ��������� ������������ �����. �� � ���� ������ �� ������������ � ������� ����������!!!


