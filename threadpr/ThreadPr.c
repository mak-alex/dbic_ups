//
//		(�) 2006, �.�.�����������. ��������� ���������� ����������� ������������ ���������� ����� ����������.
//
//		������ ����������� ����������������.
//
//		������ ������������ �� 8 ����������� ���������� ������� (�� ��� ���� "��������������" �����, ����� ��������� ����� ������).
// ������������� ��������������� ����� ��������� ����������, ����������� ������, �������� ������� ��� �������������� �� ��������
// ������������, � �� ������ ������������ ������� �� ������ ����� (�������� Sleep).
//
//		������: 1.00 �� 21.05.2006�
//
//		E-Mail: chav1961@mail.ru, chav1961@radioliga.com
//		����  : http://chav1961.narod.ru
//


#include "threadpr.h"
#define false 1
#define true 0

typedef struct 		// ��������� �������� ������ ������.
	{unsigned char		ready 		: 1;		// - ����� ����� � ���������������.
	 unsigned char		suspended 	: 1;		// - ����� � ������ ������ ����������.
	 unsigned char		waited  	: 1;		// - ����� � ������ ������ ������� ���������� �������� �����/������.
	 unsigned char		blocked 	: 1;		// - ����� � ������ ������ ������� ���������� ����������� ������.
	 unsigned char		sleeped		: 1;		// - ����� � ������ ������ ������� ���������� ���������� ���������.
	 unsigned char		reserved_1 	: 1;
	 unsigned char		reserved_2 	: 1;
	 unsigned char		reserved_3 	: 1;
	 union {unsigned short	waitMask;		// ����� ����� ������� �����/������.
			 unsigned char		*sectionId;		// ����� ����������� ������, ������� ��������� �������.
			 unsigned short	delay;			// �������� ��������� �������� (� 1/100 ���).
			} parameter;
	 unsigned short	stackPointer;			// ��������� ����� �������� ������.
	 unsigned short	stackBottom;			// ��������� ������ ����� �������� ������.
	} OneThread;
	
typedef struct			// ��������� �������� ������� ���������� �����.
	{unsigned char		existingThreads;		// 8 ��� ������������ � ������ ������ �������.
	 unsigned char		actualThread;			// �����, � ������ ������ ������� ����� ������������� �������.
	 unsigned short	ioBytes;				// ���� ������� �����-������.
	 OneThread			Threads[8];				// ��������� �� 8 �������.
	} ThreadList;
	
	
static ThreadList		Threads;				// ������� �������.


static void mt_callThread(ThreadProc *proc,void *parameter)
// ���������� ������� ���������� �����. ������������� ��� ������ ������ � ����������� ���������� ��� ����������. �������� ��� ������� �������� ��
// ������ �������� ���������! �� ���� ������� ������� �� ���������� ����������!
{	(*proc)(parameter);
	mt_deleteThread(0xFF);
	mt_yield();
}


void mt_firstThread(unsigned short stackSize,ThreadProc *proc,void *parameter)
// ������� �������������� � ������ �������������� ������, ��������� �� ���� ������ �����, � ����� ���� ���������� ���� ����������
// �������, ����� ���� ����� ���������� ���������� � ��� ������, ������ ������� ���� �������.
{unsigned short	stackTop;
 unsigned char		index;

	Threads.existingThreads = 0b0000001;				Threads.actualThread = 0;
	Threads.ioBytes = 0;								stackTop = SP;
	
	Threads.Threads[0].ready = false;					Threads.Threads[0].suspended = true;		// ��� ����� ������-������!
	Threads.Threads[0].waited = false;					Threads.Threads[0].blocked = false;
	Threads.Threads[0].sleeped	= false;				Threads.Threads[0].reserved_1 = false;
	Threads.Threads[0].reserved_2 = false;				Threads.Threads[0].reserved_3 = false;
	
	for (index = 0; index < 8; index++)	// ������������ �� ������� �������� ������.
		{Threads.Threads[index].stackBottom = stackTop;
		 stackTop -= stackSize >> 3;
		}
	
	mt_newThread(proc,parameter);			// ������ ������ ������.

	TCCR2 = 0b00000100;						// �������� ������ - ���������� ��������� ������� / 256, ������� ������ �� ������������.

	set_sleep_mode(SLEEP_MODE_IDLE);		// ���������� ������� ��������.
	while(Threads.existingThreads != 0b0000001)		// ������ �������� �������� - ���� � ������� ���� ���� ���� "��������" �����.
		{sleep_enable();
		 sleep_mode();						// ��������!					
		 sleep_disable();
		}
		
	TCCR2 = 0b00000000;						// ���������� ������.
}


unsigned char mt_newThread(ThreadProc *proc,void *parameter)
// ������� �������� ������ ������. ������� ����� � ������� ������� � ���������� ������� �������������� ����� ����� ����������� ������.
{uint8_t			forInt = SREG;	
 unsigned char		threadId = 0, threadMask;
 unsigned char		*stackAddr;

	cli();
	if (Threads.existingThreads != 0xFF)		// ��� ����� ��������� ����� �����...
		{for (threadId = 1, threadMask = 0b00000010; threadMask != 0; threadMask <<= 1, threadId++) // ����� ��������� �����...
			if ((Threads.existingThreads & threadMask) == 0) goto founded;
		 SREG = forInt; 	return 0;		// ��������� ����� ����� �� �������!
		 
founded: Threads.existingThreads |= threadMask;	// "������" �����.

		 stackAddr = (unsigned char *)(Threads.Threads[threadId].stackBottom - 36);		// �������� ��������� ����� ��� ��������� ������������� ���������.
		 Threads.Threads[threadId].stackPointer = ((unsigned short)stackAddr)-1;
		 Threads.Threads[threadId].ready = true;					Threads.Threads[threadId].suspended = false;
		 Threads.Threads[threadId].waited = false;					Threads.Threads[threadId].blocked = false;
		 Threads.Threads[threadId].sleeped	= false;				Threads.Threads[threadId].reserved_1 = false;
		 Threads.Threads[threadId].reserved_2 = false;				Threads.Threads[threadId].reserved_3 = false;
		 *stackAddr = 0;	stackAddr++;												// pop     r31 - ��� - ��� ����������� ���������� � ������ ��� ����������� ���������� �� ������� 2 � ������� mt_yield
 		 *stackAddr = 0;	stackAddr++;												// pop     r30
		 *stackAddr = 0;	stackAddr++;												// pop     r29
 		 *stackAddr = 0;	stackAddr++;												// pop     r28
		 *stackAddr = 0;	stackAddr++;												// pop     r27
 		 *stackAddr = 0;	stackAddr++;												// pop     r26
		 *stackAddr = (((unsigned short)proc) >> 8) & 0xFF;		stackAddr++;		// pop     r25 - ��������� ��� mt_callThread ����������� � ������������ ��������!
		 *stackAddr = ((unsigned short)proc) & 0xFF;				stackAddr++;		// pop     r24
		 *stackAddr = (((unsigned short)parameter) >> 8) & 0xFF;	stackAddr++;		// pop     r23
		 *stackAddr = ((unsigned short)parameter) & 0xFF;			stackAddr++;		// pop     r22
 		 *stackAddr = 0;	stackAddr++;												// pop     r21
 		 *stackAddr = 0;	stackAddr++;												// pop     r20
 		 *stackAddr = 0;	stackAddr++;												// pop     r19
 		 *stackAddr = 0;	stackAddr++;												// pop     r18
		 *stackAddr = 0;	stackAddr++;												// pop     r17
		 *stackAddr = 0;	stackAddr++;												// pop     r16
		 
		 *stackAddr = 0;	stackAddr++;												// pop     r15
		 *stackAddr = 0;	stackAddr++;												// pop     r14
		 *stackAddr = 0;	stackAddr++;												// pop     r13
		 *stackAddr = 0;	stackAddr++;												// pop     r12
		 *stackAddr = 0;	stackAddr++;												// pop     r11
		 *stackAddr = 0;	stackAddr++;												// pop     r10
		 *stackAddr = 0;	stackAddr++;												// pop     r9
		 *stackAddr = 0;	stackAddr++;												// pop     r8
		 *stackAddr = 0;	stackAddr++;												// pop     r7
		 *stackAddr = 0;	stackAddr++;												// pop     r6
		 *stackAddr = 0;	stackAddr++;												// pop     r5
		 *stackAddr = 0;	stackAddr++;												// pop     r4
		 *stackAddr = 0;	stackAddr++;												// pop     r3
		 *stackAddr = 0;	stackAddr++;												// pop     r2
		 
 		 *stackAddr = forInt;	stackAddr++;											// pop     SREG (����������)
 		 *stackAddr = 0;	stackAddr++;												// pop     r0
 		 *stackAddr = 0;	stackAddr++;												// pop     r1
		 *stackAddr = (((unsigned short)mt_callThread) >> 8) & 0xFF;	stackAddr++;	// ����� mt_callThread ��� reti (��� �� ������� - �������� ������� ������!)
		 *stackAddr = ((unsigned short)mt_callThread) & 0xFF;		stackAddr++;		// ����� mt_callThread ��� reti
		}
	SREG = forInt;		return	threadId;
}


void mt_deleteThread(unsigned char threadId)
// ������� ���������� ���������� ������.
{uint8_t			forInt = SREG;	
  
	cli();
	if (threadId == 0xFF) threadId = mt_currentThread();
	Threads.Threads[threadId].ready = false;				// ������ ����� �� ��������� ����������.
	Threads.Threads[threadId].suspended = true;
	Threads.existingThreads &= ~(1 << threadId);			// ������� ����� �� ������ ������� �������.
	SREG = forInt;
}


unsigned char mt_currentThread(void)
// ������� ������ �������������� �������� ������.
{uint8_t			forInt = SREG;
 unsigned char		result;
  
	cli();			result = Threads.actualThread;			SREG = forInt;
	return			result;
}


void mt_suspendThread(unsigned char threadId)
// ������� ��������� ��������� ���������� ������. ����� ����������� � ���������������� ��������� ���������� �� ��� �������� �������.
{uint8_t			forInt = SREG;	
  
	cli();
	if (threadId == 0xFF) threadId = mt_currentThread();
	Threads.Threads[threadId].ready = false;
	Threads.Threads[threadId].suspended = true;
	mt_yield();
	SREG = forInt;
}


void mt_resumeThread(unsigned char threadId)
// ������� ������������� ����� �������������� ������. ����� ����������� � ��������� ���������� ������ ���� � ��� �� ���� �����-���� ��������� ��������.
{uint8_t			forInt = SREG;	
  
	cli();
	if (threadId == 0xFF) threadId = mt_currentThread();
	Threads.Threads[threadId].suspended = false;
	if (!(Threads.Threads[threadId].suspended || Threads.Threads[threadId].waited || Threads.Threads[threadId].blocked || Threads.Threads[threadId].sleeped))
		{Threads.Threads[threadId].ready = true;	mt_yield();}
	SREG = forInt;
}


void mt_prepareSection(unsigned char *sectionAddr){*sectionAddr = 0;}
// ������� ���������� � ������ ����� ����������� ������


void mt_lock(unsigned char *sectionAddr)
// ���������� ����������� ������.
{uint8_t			forInt = SREG;	
 unsigned char		threadId;
  
	cli();
	if (*sectionAddr != 0)	// ��� ������ ��� ����!
		{*sectionAddr = 1 << (threadId = mt_currentThread());	// �n������ ����, ��� ��� ������ ��� ���� �����!
		 Threads.Threads[threadId].ready = false;				Threads.Threads[threadId].blocked = true;
 		 Threads.Threads[threadId].parameter.sectionId = sectionAddr;
		 mt_yield();
		}
	else *sectionAddr = 1 << mt_currentThread();
	SREG = forInt;
}


void mt_unlock(unsigned char *sectionAddr)
// ����� �� ����������� ������.
{uint8_t			forInt = SREG;	
 unsigned char		iPoz, curThread, threadId;
  
	cli();			
	if ((*sectionAddr &= ~(1 << (curThread = mt_currentThread()))) != 0)	// ��� ������ ���-�� ����!
		{for (iPoz = 1; iPoz <= 8; iPoz++)						// �������� ������ ������� �� ������� �������� ���� ������.
			{threadId = (curThread + iPoz) & 0x07;
			 if (Threads.Threads[threadId].blocked && Threads.Threads[threadId].parameter.sectionId == sectionAddr)
				{Threads.Threads[threadId].blocked = false;
				 if (!(Threads.Threads[threadId].suspended || Threads.Threads[threadId].waited || Threads.Threads[threadId].blocked || Threads.Threads[threadId].sleeped))
					{Threads.Threads[threadId].ready = true;	mt_yield();		break;}
				}
			}
		}
	SREG = forInt;
}


void mt_prepareEvent(unsigned short wait){Threads.ioBytes &= ~wait;}
// ���������� ���������� ������� � ��������� �������� �������������.


unsigned short mt_wait(unsigned short wait)
// �������� ���������� �������� �����/������.
{uint8_t			forInt = SREG;	
 unsigned short	result = -1;

	cli();
	if ((Threads.ioBytes & wait) == 0)			// ������� ��� �� ���� ��������.
		{Threads.Threads[Threads.actualThread].ready = false;
		 Threads.Threads[Threads.actualThread].waited = true;
 		 Threads.Threads[Threads.actualThread].parameter.waitMask = wait;
		 mt_yield();
		 result = Threads.Threads[Threads.actualThread].parameter.waitMask;
		}
	else mt_prepareEvent(wait);				// ������� ��� ���������!
	SREG = forInt;
	return	result;
}


void mt_notify(unsigned short wait)
// ������� ���������� �������.
{uint8_t			forInt = SREG;	
 unsigned char		iPoz;

	cli();
	for (iPoz = 0; iPoz < 8; iPoz++)						// �������� ������ ������� �� ������� �������� ����� �������.
		{if (Threads.Threads[iPoz].waited && (Threads.Threads[iPoz].parameter.waitMask & wait) != 0)
			{Threads.Threads[iPoz].waited = false;
			 Threads.Threads[Threads.actualThread].parameter.waitMask = wait;	// �������, ����� ������ ������� ���������.
			 if (!(Threads.Threads[iPoz].suspended || Threads.Threads[iPoz].waited || Threads.Threads[iPoz].blocked || Threads.Threads[iPoz].sleeped))
				{Threads.Threads[iPoz].ready = true;	SREG = forInt;		return;}
			}
		}
	Threads.ioBytes |= wait;								// ��� ������� ��� ����� �� ���� - �������� ���� ��������!
	SREG = forInt;
}


void mt_sleep(unsigned short second_cent)
// �������� ��������� ���������� ���������.
{
uint8_t			forInt = SREG;	
	
	cli();
	Threads.Threads[Threads.actualThread].ready = false;
	Threads.Threads[Threads.actualThread].sleeped = true;
	Threads.Threads[Threads.actualThread].parameter.delay = second_cent;
	mt_yield();	
	SREG = forInt;
}


void __attribute__ ((naked)) mt_yield(void) 		// �� warning ��� ���������� �� �������� ��������!
// ������������ ���������� �� ������ �����. ������ ������� ������ ��������� � ������������ ��������� ����������, ������ ��� ��� ����������� ��� ������ ���� ���������!!!
{	__asm__
		("\tpush __zero_reg__\n"
		 "\tpush __tmp_reg__\n"
		 "\tin __tmp_reg__,__SREG__\n"
		 "\tpush __tmp_reg__\n"
		 "\tclr __zero_reg__\n"
		 "\tpush r2\n"
		 "\tpush r3\n"
		 "\tpush r4\n"
		 "\tpush r5\n"
		 "\tpush r6\n"
		 "\tpush r7\n"
		 "\tpush r8\n"
		 "\tpush r9\n"
		 "\tpush r10\n"
		 "\tpush r11\n"
		 "\tpush r12\n"
		 "\tpush r13\n"
		 "\tpush r14\n"
		 "\tpush r15\n"
		 "\tpush r16\n"
 		 "\tpush r17\n"
 		 "\tpush r18\n"
 		 "\tpush r19\n"
 		 "\tpush r20\n"
 		 "\tpush r21\n"
 		 "\tpush r22\n"
 		 "\tpush r23\n"
 		 "\tpush r24\n"
 		 "\tpush r25\n"
 		 "\tpush r26\n"
 		 "\tpush r27\n"
 		 "\tpush r28\n"
 		 "\tpush r29\n"
 		 "\tpush r30\n"
 		 "\tpush r31\n"
 		);
	cli();											// ��� ������� ����� ���������� � �� �������� ������������, ������� ���������� � ��� ���������� ���������!
	
	Threads.Threads[Threads.actualThread].stackPointer = SP;		// ���������� �������� ������������ �����.
	__mt_yield();
	SP = Threads.Threads[Threads.actualThread].stackPointer;

	__asm__
		("\tpop r31\n"
		 "\tpop r30\n"
		 "\tpop r29\n"
		 "\tpop r28\n"
		 "\tpop r27\n"
		 "\tpop r26\n"
		 "\tpop r25\n"
		 "\tpop r24\n"
		 "\tpop r23\n"
		 "\tpop r22\n"
		 "\tpop r21\n"
		 "\tpop r20\n"
		 "\tpop r19\n"
		 "\tpop r18\n"
		 "\tpop r17\n"
		 "\tpop r16\n"
		 "\tpop r15\n"
		 "\tpop r14\n"
		 "\tpop r13\n"
		 "\tpop r12\n"
		 "\tpop r11\n"
		 "\tpop r10\n"
		 "\tpop r9\n"
		 "\tpop r8\n"
		 "\tpop r7\n"
		 "\tpop r6\n"
		 "\tpop r5\n"
		 "\tpop r4\n"
		 "\tpop r3\n"
		 "\tpop r2\n"		 
		 "\tpop __tmp_reg__\n"
		 "\tout __SREG__,__tmp_reg__\n"
		 "\tpop __tmp_reg__\n"
		 "\tpop __zero_reg__\n"
		 "\treti\n"
		);
}


void __mt_VirtualTimer(void)
// ��������� ����������� ��������.
{unsigned char forYield2;

	for (forYield2 = 0; forYield2 < 8; forYield2++)
		if (Threads.Threads[forYield2].sleeped)	// ���� ������, ��������� ��������� ��������.
			{if (Threads.Threads[forYield2].parameter.delay == 0)	// ��������� �������� �������!
				{Threads.Threads[forYield2].sleeped = false;				 
				 if (!(Threads.Threads[forYield2].suspended || Threads.Threads[forYield2].waited || Threads.Threads[forYield2].blocked || Threads.Threads[forYield2].sleeped))
					Threads.Threads[forYield2].ready = true;
				}
			 else Threads.Threads[forYield2].parameter.delay--;
			}
}


void __attribute__ ((naked)) TIMER2_OVF_vect(void)
// ��������� ����� �������. ������ ������� ������ ��������� ��������� ����������, ������ ��� ��� ��� ������ ����������� ���� ���������!!!
{	__asm__
		("\tpush __zero_reg__\n"
		 "\tpush __tmp_reg__\n"
		 "\tin __tmp_reg__,__SREG__\n"
		 "\tpush __tmp_reg__\n"
		 "\tclr __zero_reg__\n"
		 "\tpush r2\n"
		 "\tpush r3\n"
		 "\tpush r4\n"
		 "\tpush r5\n"
		 "\tpush r6\n"
		 "\tpush r7\n"
		 "\tpush r8\n"
		 "\tpush r9\n"
		 "\tpush r10\n"
		 "\tpush r11\n"
		 "\tpush r12\n"
		 "\tpush r13\n"
		 "\tpush r14\n"
		 "\tpush r15\n"
		 "\tpush r16\n"
 		 "\tpush r17\n"
 		 "\tpush r18\n"
 		 "\tpush r19\n"
 		 "\tpush r20\n"
 		 "\tpush r21\n"
 		 "\tpush r22\n"
 		 "\tpush r23\n"
 		 "\tpush r24\n"
 		 "\tpush r25\n"
 		 "\tpush r26\n"
 		 "\tpush r27\n"
 		 "\tpush r28\n"
 		 "\tpush r29\n"
 		 "\tpush r30\n"
 		 "\tpush r31\n"
 		);

	__mt_VirtualTimer();
	
	Threads.Threads[Threads.actualThread].stackPointer = SP;		// ���������� �������� ������������ �����.
	__mt_yield();
	SP = Threads.Threads[Threads.actualThread].stackPointer;
	
	__asm__
		("\tpop r31\n"
		 "\tpop r30\n"
		 "\tpop r29\n"
		 "\tpop r28\n"
		 "\tpop r27\n"
		 "\tpop r26\n"
		 "\tpop r25\n"
		 "\tpop r24\n"
		 "\tpop r23\n"
		 "\tpop r22\n"
		 "\tpop r21\n"
		 "\tpop r20\n"
		 "\tpop r19\n"
		 "\tpop r18\n"
		 "\tpop r17\n"
		 "\tpop r16\n"
		 "\tpop r15\n"
		 "\tpop r14\n"
		 "\tpop r13\n"
		 "\tpop r12\n"
		 "\tpop r11\n"
		 "\tpop r10\n"
		 "\tpop r9\n"
		 "\tpop r8\n"
		 "\tpop r7\n"
		 "\tpop r6\n"
		 "\tpop r5\n"
		 "\tpop r4\n"
		 "\tpop r3\n"
		 "\tpop r2\n"		 
		 "\tpop __tmp_reg__\n"
		 "\tout __SREG__,__tmp_reg__\n"
		 "\tpop __tmp_reg__\n"
		 "\tpop __zero_reg__\n"
		 "\treti\n"
		);
}

void __mt_yield(void)
// ���������� ��������� ������������ �����. �� � ���� ������ �� ������������ � ������� ����������!!! �������� ������������ ����� - ����������� �� �������� ������.
{unsigned char		iPoz, threadId;

	for (iPoz = 1; iPoz <= 8; iPoz++)				// �������� "����������" �������.
		{threadId = (Threads.actualThread + iPoz) & 0x07;
		 if (Threads.Threads[threadId].ready)		// ���� ������� � ���������� �����!
			{Threads.actualThread = threadId;		return;}
		}
	Threads.actualThread = 0;		// ���������� ������� �� ����� - ��������� � �����-������!
}
